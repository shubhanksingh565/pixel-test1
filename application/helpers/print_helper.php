<?php

function pre($data, $exit = '')
{
   echo "<pre>";
   print_r($data);

   if (!empty($exit)) {
      return exit();
   }
}
