<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php 
	$this->load->view('Common/links');
	?>
	<title>Pixel| Api Data</title>
</head>
<body>
<?php
	$this->load->view('Common/header');
?>
<br>
<section class="container">
<h3 align="center">Sample webpage to show price data</h3><hr style="border: 2px solid black;background: black;"/>
<div class="price">
	
</div>
</section>

<script>
	const url = "<?=site_url()?>" + "dashboard/apiData";
	$.ajax({
			type: "POST",
			url: url,
			success: function(result){
			$(".price").html(result);
			}
		});
	setInterval(() =>{
		$.ajax({
			type: "POST",
			url: url,
			success: function(result){
			$(".price").html(result);
			}
		});
}, 10000);


	
</script>
</body>
</html>