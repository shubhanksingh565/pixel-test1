<?php
class Appmodel extends CI_Model
{



  public function insert_one($table, $data)
  {
    $this->db->insert($table, $data);
    if ($this->db->affected_rows()) {
      return $this->db->insert_id();
    } else {
      return false;
    }
  }

  public function select_all($table, $where, $queries = [])
  {

    $select = '*';
    if (isset($queries['select'])) {
      $select = $queries['select'];
    }

    $this->db->select($select);

    if (isset($queries['joins'])) {
      foreach ($queries['joins'] as $join) {
        $this->db->join($join['table'], $join['condition'], $join['joinType']);
      }
    }

    if ($where) {
      $this->db->where($where);
    }

    if (isset($queries['where_in']) && !empty($queries['where_in'])) {
      $this->db->where_in($queries['where_in'][0], $queries['where_in'][1]);
    }

    if (isset($queries['like']) && !empty($queries['like'])) {
      $this->db->where($queries['like']);
    }

    if (isset($queries['limit'])) {
      $this->db->limit($queries['limit'][1], $queries['limit'][0]);
    }

    if (isset($queries['order'])) {
      $this->db->order_by($queries['order'][0], $queries['order'][1]);
    }



    $query = $this->db->from($table)->get();
    $data = $query->result();
    $rows = $query->num_rows();

    $return = [];
    if (isset($queries['return'])) {
      $return = $queries['return'];
    }

    if ($return == 'both') {
      $return = [
        'data'  => $data,
        'rows'  => $rows
      ];
    } elseif ($return == 'rows') {
      $return = $rows;
    } else {
      $return = $data;
    }
    return $return;
  }


  public function select_one($table, $where, $queries = [])
  {

    $select = '*';
    if (isset($queries['select'])) {
      $select = $queries['select'];
    }

    $this->db->select($select);

    if (isset($queries['joins'])) {
      foreach ($queries['joins'] as $join) {
        $this->db->join($join['table'], $join['condition'], $join['joinType']);
      }
    }

    if ($where) {
      $this->db->where($where);
    }

    if (isset($queries['like']) && !empty($queries['like'])) {
      $this->db->where($queries['like']);
    }
    if (isset($queries['order'])) {
      $this->db->order_by($queries['order'][0], $queries['order'][1]);
    }
    if (isset($queries['limit'])) {
      $this->db->limit($queries['limit'][1], $queries['limit'][0]);
    }



    $query = $this->db->from($table)->get();
    $data = $query->row();
    $rows = $query->num_rows();

    $return = [];
    if (isset($queries['return'])) {
      $return = $queries['return'];
    }

    if ($return == 'both' && $data && $rows) {
      $return = [
        'data'  => $data,
        'rows'  => $rows
      ];
    } elseif ($return == 'rows' && $rows) {
      $return = $rows;
    } elseif ($data) {
      $return = $data;
    }
    return $return;
  }
}
