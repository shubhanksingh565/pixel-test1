<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function index()
	{
		$this->load->view('Dashboard/index');
	}
	public function apiData()
	{
		date_default_timezone_set("Asia/Calcutta");
		$filter = [
			'order' => ['id', 'desc']
		];
		$bidata = $this->appmodel->select_one('bids', [], $filter);
		// pre($bidata,1);
		$currentdatetime  = date('Y-m-d H:i:s');
		$second = strtotime($currentdatetime) - strtotime($bidata->time);
		if (empty($bidata)) {
			$returnData = $this->callApi();
			if ($returnData['status'] == 1 &&  $returnData['message'] == 'Success') {
				$insertData = [
					'symbol' => $returnData['data']['symbol'],
					'bidPrice' => $returnData['data']['bidPrice'],
					'bidQty' => $returnData['data']['bidQty'],
					'askPrice' => $returnData['data']['askPrice'],
					'askQty' => $returnData['data']['askQty'],
					'time' => date("Y-m-d H:i:s"),
					'apiCallCount' => 1,
				];
				$return =   $this->appmodel->insert_one('bids', $insertData);
			} else {
				echo "Failed To call Api";
			}
		} elseif ($second >= 30) {
			$returnData = $this->callApi();
			if ($returnData['status'] == 1 &&  $returnData['message'] == 'Success') {
				$insertData = [
					'symbol' => $returnData['data']['symbol'],
					'bidPrice' => $returnData['data']['bidPrice'],
					'bidQty' => $returnData['data']['bidQty'],
					'askPrice' => $returnData['data']['askPrice'],
					'askQty' => $returnData['data']['askQty'],
					'time' => date("Y-m-d H:i:s"),
					'apiCallCount' => $bidata->apiCallCount + 1,
				];
				$return =   $this->appmodel->insert_one('bids', $insertData);
			} else {
				echo "Failed To call Api";
			}
		}
		$filter = [
			'limit' => [0, 10],
			'order' => ['id', 'desc']
		];
		$data = $this->appmodel->select_all('bids', [], $filter);
		//$expbid = excplode('.',$bidata->bidPrice);
		if ($bidata->bidPrice >= 1) {
			$bidprice = number_format((float)$bidata->bidPrice, 2, '.', '');
		} else {
			$bidprice = number_format((float)$bidata->bidPrice, 7, '.', '');
		}
		$output = '';
		$output .= "
			<h4>Latest Price from Coin Matket Cap</h4>
			<h4>Connect Status : <b>Connection Established</b></h4>
			<h4>Latest Price BTC : <b>" . $bidprice . "</b></h4>
		";
		$output .= "<table class='table'><tr>
					<th>SN.</th>
					<th>Symbol</th>
					<th>Bid Price</th>
					<th>Bid Quantity</th>
					<th>Ask Price</th>
					<th>Ask Quantity</th>
					<th>Api Call Time</th>
					<th>Call Count</th>
				</tr>
			";
		$i = 1;
		foreach ($data as $d) {
			$bidPrice = ($d->bidPrice >= 1) ? number_format((float)$d->bidPrice, 2, '.', '') : number_format((float)$d->bidPrice, 7, '.', '');
			$askPrice = ($d->bidPrice >= 1) ? number_format((float)$d->askPrice, 2, '.', '') : number_format((float)$d->askPrice, 7, '.', '');

			$output .= "<tr>
					<td>" . $i++ . "</td>
					<td>" . $d->symbol . "</td>
					<td>" . $bidPrice . "</td>
					<td>" . $d->bidQty . "</td>
					<td>" . $askPrice . "</td>
					<td>" . $d->askQty . "</td>
					<td>" . $d->time . "</td>
					<td>" . $d->apiCallCount . "</td>
				</tr>
			";
		}
		$output .= "</table>";
		echo $output;
		die;
	}

	
	public function callApi()
	{
		$url = "https://dev.pixelsoftwares.com/api.php";
		$token = "ab4086ecd47c568d5ba5739d4078988f";

		$postData = array(
			"symbol" => "BTCUSDT",
		);

		// for sending data as json type
		$fields = json_encode($postData);

		$ch = curl_init($url);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'token: ' . $token // if you need token in header
			)
		);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result =  json_decode($result, true);
		die;
	}
}
